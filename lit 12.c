#include <stdio.h>
int main()
{
  float r, area;

  printf("Enter the radius of a circle\n");

  scanf("%f", &r);

  area = 3.14159*r*r;

  printf("Area of the circle = %.2f\n", area);  // printing upto two decimal places

  return 0;
}
